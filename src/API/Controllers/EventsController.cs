﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using CountryFriendStatistics.Database;
using CountryFriendStatistics.Database.Models.Events;
using CountryFriendStatistics.Database.Models.Events.Base;
using ExtendedXmlSerializer.Configuration;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CountryFriendStatistics.API.Controllers
{
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly CountryFriendDbContext _context;

        public EventsController(CountryFriendDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("api/user/{userId}/adevents")]
        public string GetUserAdEvents(int userId, [FromQuery(Name = "serializationType")] string serializationType)
        {
            IEnumerable<AdEvent> gameEvents = _context.Users.FirstOrDefault(x => x.Id == userId).AdEvents;

            switch (serializationType)
            {
                case "json":
                    return JsonConvert.SerializeObject(gameEvents);
                case "xml":
                    {
                        var gameEventsList = gameEvents
                            .Select(x => new AdEventViewModel
                            {
                                Id = x.Id,
                                EventTime = x.EventTime
                            })
                            .ToList();

                        return SerializeXml(new XmlSerializer(gameEventsList.GetType()), gameEventsList);
                    }
                case "csv":
                    return ServiceStack.Text.CsvSerializer.SerializeToCsv(gameEvents);
                default: return null;
            }
        }

        [HttpGet]
        [Route("api/user/{userId}/builddecoratives")]
        public string GetUserBuildDecorativesEvents(int userId, [FromQuery(Name = "serializationType")] string serializationType)
        {
            IEnumerable<BuildDecorativeEvent> gameEvents = _context.Users
                .FirstOrDefault(x => x.Id == userId).BuildDecorativeEvents;

            switch (serializationType)
            {
                case "json":
                    return JsonConvert.SerializeObject(gameEvents);
                case "xml":
                    {
                        var gameEventsList = gameEvents
                            .Select(x => new BuildDecorativeEventViewModel
                            {
                                Id = x.Id,
                                EventTime = x.EventTime,
                                BuildingType = x.BuildingType
                            })
                            .ToList();

                        return SerializeXml(new XmlSerializer(gameEventsList.GetType()), gameEventsList);
                    }
                case "csv":
                    return ServiceStack.Text.CsvSerializer.SerializeToCsv(gameEvents);
                default: return null;
            }
        }

        [HttpGet]
        [Route("api/user/{userId}/build")]
        public string GetUserBuildEvents(int userId, [FromQuery(Name = "serializationType")] string serializationType)
        {
            IEnumerable<BuildEvent> gameEvents = _context.Users
                .FirstOrDefault(x => x.Id == userId).BuildEvents;

            switch (serializationType)
            {
                case "json":
                    return JsonConvert.SerializeObject(gameEvents);
                case "xml":
                    {
                        var gameEventsList = gameEvents
                            .Select(x => new BuildEvent
                            {
                                Id = x.Id,
                                EventTime = x.EventTime,
                                BuildingType = x.BuildingType
                            })
                            .ToList();

                        return SerializeXml(new XmlSerializer(gameEventsList.GetType()), gameEventsList);
                    }
                case "csv":
                    return ServiceStack.Text.CsvSerializer.SerializeToCsv(gameEvents);
                default: return null;
            }
        }


        [HttpGet]
        [Route("api/user/{userId}/donat")]
        public string GetUserDonutEvents(int userId, [FromQuery(Name = "serializationType")] string serializationType)
        {
            IEnumerable<DonatEvent> gameEvents = _context.Users
                .FirstOrDefault(x => x.Id == userId).DonatEvents;

            switch (serializationType)
            {
                case "json":
                    return JsonConvert.SerializeObject(gameEvents);
                case "xml":
                    {
                        var gameEventsList = gameEvents
                            .Select(x => new DonatEvent
                            {
                                Id = x.Id,
                                EventTime = x.EventTime,
                                Amount = x.Amount
                            })
                            .ToList();

                        return SerializeXml(new XmlSerializer(gameEventsList.GetType()), gameEventsList);
                    }
                case "csv":
                    return ServiceStack.Text.CsvSerializer.SerializeToCsv(gameEvents);
                default: return null;
            }
        }

        [HttpGet]
        [Route("api/user/{userId}/gameexit")]
        public string GetUserGameExitEvents(int userId, [FromQuery(Name = "serializationType")] string serializationType)
        {
            IEnumerable<GameExitEvent> gameEvents = _context.Users
                .FirstOrDefault(x => x.Id == userId).GameExitEvents;

            switch (serializationType)
            {
                case "json":
                    return JsonConvert.SerializeObject(gameEvents);
                case "xml":
                    {
                        var gameEventsList = gameEvents
                            .Select(x => new GameExitEvent
                            {
                                Id = x.Id,
                                EventTime = x.EventTime
                            })
                            .ToList();

                        return SerializeXml(new XmlSerializer(gameEventsList.GetType()), gameEventsList);
                    }
                case "csv":
                    return ServiceStack.Text.CsvSerializer.SerializeToCsv(gameEvents);
                default: return null;
            }
        }

        [HttpGet]
        [Route("api/user/{userId}/gamelaunch")]
        public string GetUserGameLaunchEvents(int userId, [FromQuery(Name = "serializationType")] string serializationType)
        {
            IEnumerable<GameLaunchEvent> gameEvents = _context.Users
                .FirstOrDefault(x => x.Id == userId).GameLaunchEvents;

            switch (serializationType)
            {
                case "json":
                    return JsonConvert.SerializeObject(gameEvents);
                case "xml":
                    {
                        var gameEventsList = gameEvents
                            .Select(x => new GameLaunchEvent
                            {
                                Id = x.Id,
                                EventTime = x.EventTime
                            })
                            .ToList();

                        return SerializeXml(new XmlSerializer(gameEventsList.GetType()), gameEventsList);
                    }
                case "csv":
                    return ServiceStack.Text.CsvSerializer.SerializeToCsv(gameEvents);
                default: return null;
            }
        }

        [HttpGet]
        [Route("api/user/{userId}/processacceleration")]
        public string GetUserProcessAccelerationEvents(int userId, [FromQuery(Name = "serializationType")] string serializationType)
        {
            IEnumerable<ProcessAccelerationEvent> gameEvents = _context.Users
                .FirstOrDefault(x => x.Id == userId).ProcessAccelerationEvents;

            switch (serializationType)
            {
                case "json":
                    return JsonConvert.SerializeObject(gameEvents);
                case "xml":
                    {
                        var gameEventsList = gameEvents
                            .Select(x => new ProcessAccelerationEvent
                            {
                                Id = x.Id,
                                EventTime = x.EventTime,
                                ProcessType = x.ProcessType
                            })
                            .ToList();

                        return SerializeXml(new XmlSerializer(gameEventsList.GetType()), gameEventsList);
                    }
                case "csv":
                    return ServiceStack.Text.CsvSerializer.SerializeToCsv(gameEvents);
                default: return null;
            }
        }

        [HttpGet]
        [Route("api/user/{userId}/resourceharvest")]
        public string GetUserResourceHarvestEvents(int userId, [FromQuery(Name = "serializationType")] string serializationType)
        {
            IEnumerable<ResourceHarvestEvent> gameEvents = _context.Users
                .FirstOrDefault(x => x.Id == userId).ResourceHarvestEvents;

            switch (serializationType)
            {
                case "json":
                    return JsonConvert.SerializeObject(gameEvents);
                case "xml":
                    {
                        var gameEventsList = gameEvents
                            .Select(x => new ResourceHarvestEvent
                            {
                                Id = x.Id,
                                EventTime = x.EventTime,
                                ResourceType = x.ResourceType
                            })
                            .ToList();

                        return SerializeXml(new XmlSerializer(gameEventsList.GetType()), gameEventsList);
                    }
                case "csv":
                    return ServiceStack.Text.CsvSerializer.SerializeToCsv(gameEvents);
                default: return null;
            }
        }

        [HttpGet]
        [Route("api/user/{userId}/resourceselling")]
        public string GetUserResourceSellingEvents(int userId, [FromQuery(Name = "serializationType")] string serializationType)
        {
            IEnumerable<ResourceSellingEvent> gameEvents = _context.Users
                .FirstOrDefault(x => x.Id == userId).ResourceSellingEvents;

            switch (serializationType)
            {
                case "json":
                    return JsonConvert.SerializeObject(gameEvents);
                case "xml":
                    {
                        var gameEventsList = gameEvents
                            .Select(x => new ResourceSellingEvent
                            {
                                Id = x.Id,
                                EventTime = x.EventTime,
                                ResourceType = x.ResourceType
                            })
                            .ToList();

                        return SerializeXml(new XmlSerializer(gameEventsList.GetType()), gameEventsList);
                    }
                case "csv":
                    return ServiceStack.Text.CsvSerializer.SerializeToCsv(gameEvents);
                default: return null;
            }
        }

        private string SerializeXml(XmlSerializer xmlSerializer, object obj)
        {
            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, obj);
                return (textWriter.ToString());
            }
        }
    }
}
