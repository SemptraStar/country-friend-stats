using System;
using CountryFriendStatistics.Database.Models.Events.Base;
using CountryFriendStatistics.Database.Models.Events.Enums;

namespace CountryFriendStatistics.Database.Models.Events
{
    public class AdEventViewModel
    {
        public int Id;

        public DateTime EventTime;
    }
}