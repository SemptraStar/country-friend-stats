using CountryFriendStatistics.Database.Models.Events.Base;
using CountryFriendStatistics.Database.Models.Events.Enums;
using System;

namespace CountryFriendStatistics.Database.Models.Events
{
    public class BuildEventViewModel
    {
        public int Id;

        public DateTime EventTime;

        public BuildingType BuildingType;
    }
}