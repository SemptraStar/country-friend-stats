using CountryFriendStatistics.Database.Models.Events.Base;
using CountryFriendStatistics.Database.Models.Events.Enums;
using System;

namespace CountryFriendStatistics.Database.Models.Events
{
    public class DonatEventViewModel
    {
        public int Id;

        public DateTime EventTime;

        public int Amount;
    }
}