using CountryFriendStatistics.Database.Models.Events.Base;
using System;

namespace CountryFriendStatistics.Database.Models.Events
{
    public class GameLaunchEventViewModel
    {
        public int Id;

        public DateTime EventTime;
    }
}