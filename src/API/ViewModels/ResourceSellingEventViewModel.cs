using System;
using CountryFriendStatistics.Database.Models.Events.Base;
using CountryFriendStatistics.Database.Models.Events.Enums;

namespace CountryFriendStatistics.API.VieModels
{
    public class ResourceSellingEventViewModel
    {
        public int Id;

        public DateTime EventTime;

        public ResourceType ResourceType;
    }
}