using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CountryFriendStatistics.Database.Models.Events;
using CountryFriendStatistics.Database.Models.Users;
using Microsoft.EntityFrameworkCore;

namespace CountryFriendStatistics.Database
{
    public class CountryFriendDbContext : DbContext
    {
        public CountryFriendDbContext(DbContextOptions<CountryFriendDbContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<AdEvent> AdEvents { get; set; }

        public DbSet<BuildDecorativeEvent> BuildDecorativeEvents { get; set; }

        public DbSet<BuildEvent> BuildEvents { get; set; }

        public DbSet<DonatEvent> DonatEvents { get; set; }

        public DbSet<GameExitEvent> GameExitEvents { get; set; }

        public DbSet<GameLaunchEvent> GameLaunchEvents { get; set; }

        public DbSet<ProcessAccelerationEvent> ProcessAccelerationEvents { get; set; }

        public DbSet<ResourceHarvestEvent> ResourceHarvestEvents { get; set; }

        public DbSet<ResourceSellingEvent> ResourceSellingEvents { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasIndex(x => x.Id);
            modelBuilder.Entity<AdEvent>().HasIndex(x => x.Id);
            modelBuilder.Entity<BuildDecorativeEvent>().HasIndex(x => x.Id);
            modelBuilder.Entity<BuildEvent>().HasIndex(x => x.Id);
            modelBuilder.Entity<DonatEvent>().HasIndex(x => x.Id);
            modelBuilder.Entity<GameExitEvent>().HasIndex(x => x.Id);
            modelBuilder.Entity<GameLaunchEvent>().HasIndex(x => x.Id);
            modelBuilder.Entity<ProcessAccelerationEvent>().HasIndex(x => x.Id);
            modelBuilder.Entity<ResourceHarvestEvent>().HasIndex(x => x.Id);
            modelBuilder.Entity<ResourceSellingEvent>().HasIndex(x => x.Id);

            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    Email = "oleksii.sachek@nure.ua"
                },
                new User
                {
                    Id = 2,
                    Email = "alexeycacheknew@gmail.com"
                }
            );
        }
    }
}