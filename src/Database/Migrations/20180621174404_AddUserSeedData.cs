﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountryFriendStatistics.Database.Migrations
{
    public partial class AddUserSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email" },
                values: new object[] { 1, "oleksii.sachek@nure.ua" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email" },
                values: new object[] { 2, "alexeycacheknew@gmail.com" });

            migrationBuilder.CreateIndex(
                name: "IX_Users_Id",
                table: "Users",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ResourceSellingEvents_Id",
                table: "ResourceSellingEvents",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ResourceHarvestEvents_Id",
                table: "ResourceHarvestEvents",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProcessAccelerationEvents_Id",
                table: "ProcessAccelerationEvents",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_GameLaunchEvents_Id",
                table: "GameLaunchEvents",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_GameExitEvents_Id",
                table: "GameExitEvents",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_DonatEvents_Id",
                table: "DonatEvents",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_BuildEvents_Id",
                table: "BuildEvents",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_BuildDecorativeEvents_Id",
                table: "BuildDecorativeEvents",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_AdEvents_Id",
                table: "AdEvents",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Users_Id",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_ResourceSellingEvents_Id",
                table: "ResourceSellingEvents");

            migrationBuilder.DropIndex(
                name: "IX_ResourceHarvestEvents_Id",
                table: "ResourceHarvestEvents");

            migrationBuilder.DropIndex(
                name: "IX_ProcessAccelerationEvents_Id",
                table: "ProcessAccelerationEvents");

            migrationBuilder.DropIndex(
                name: "IX_GameLaunchEvents_Id",
                table: "GameLaunchEvents");

            migrationBuilder.DropIndex(
                name: "IX_GameExitEvents_Id",
                table: "GameExitEvents");

            migrationBuilder.DropIndex(
                name: "IX_DonatEvents_Id",
                table: "DonatEvents");

            migrationBuilder.DropIndex(
                name: "IX_BuildEvents_Id",
                table: "BuildEvents");

            migrationBuilder.DropIndex(
                name: "IX_BuildDecorativeEvents_Id",
                table: "BuildDecorativeEvents");

            migrationBuilder.DropIndex(
                name: "IX_AdEvents_Id",
                table: "AdEvents");

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
