using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using CountryFriendStatistics.Database.Models.Users;
using Newtonsoft.Json;

namespace CountryFriendStatistics.Database.Models.Events.Base
{
    public class GameEvent
    {
        [Key]
        public int Id { get; set; }

        public DateTime EventTime { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public int UserId { get; set; }
        [XmlIgnore]
        [JsonIgnore]
        public virtual User User { get; set; }
    }
}