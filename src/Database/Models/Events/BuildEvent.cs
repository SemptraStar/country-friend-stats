using CountryFriendStatistics.Database.Models.Events.Base;
using CountryFriendStatistics.Database.Models.Events.Enums;

namespace CountryFriendStatistics.Database.Models.Events
{
    public class BuildEvent : GameEvent
    {
        public BuildingType BuildingType { get; set; }
    }
}