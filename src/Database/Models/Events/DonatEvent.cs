using CountryFriendStatistics.Database.Models.Events.Base;
using CountryFriendStatistics.Database.Models.Events.Enums;

namespace CountryFriendStatistics.Database.Models.Events
{
    public class DonatEvent : GameEvent
    {
        public int Amount { get; set; }
    }
}