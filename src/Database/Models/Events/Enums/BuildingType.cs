namespace CountryFriendStatistics.Database.Models.Events.Enums
{
    public enum BuildingType
    {
        CowsPasture,
        Field,
        ChickenCoop,
        Pigsty,
        Sheepfold,
        Fence,
        Pond,
        Path,
        Bush
    }
}