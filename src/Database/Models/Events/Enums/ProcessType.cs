namespace CountryFriendStatistics.Database.Models.Events.Enums
{
    public enum ProcessType
    {
        Building,
        WaitingForResources,
        WaitingForRecycling,
        Quest
    }
}