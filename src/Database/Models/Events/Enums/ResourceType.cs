namespace CountryFriendStatistics.Database.Models.Events.Enums
{
    public enum ResourceType
    {
        Cow,
        Chicken,
        Pig,
        Sheep,
        Wheat,
        Tomatoes,
        Apples
    }
}