using CountryFriendStatistics.Database.Models.Events.Base;
using CountryFriendStatistics.Database.Models.Events.Enums;

namespace CountryFriendStatistics.Database.Models.Events
{
    public class ProcessAccelerationEvent : GameEvent
    {
        public ProcessType ProcessType { get; set; }
    }
}