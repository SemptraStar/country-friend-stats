using CountryFriendStatistics.Database.Models.Events.Base;
using CountryFriendStatistics.Database.Models.Events.Enums;

namespace CountryFriendStatistics.Database.Models.Events
{
    public class ResourceHarvestEvent : GameEvent
    {
        public ResourceType ResourceType { get; set; }
    }
}