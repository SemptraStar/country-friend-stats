using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CountryFriendStatistics.Database.Models.Events;

namespace CountryFriendStatistics.Database.Models.Users
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public virtual ICollection<AdEvent> AdEvents { get; set; }

        public virtual ICollection<BuildDecorativeEvent> BuildDecorativeEvents { get; set; }

        public virtual ICollection<BuildEvent> BuildEvents { get; set; }

        public virtual ICollection<DonatEvent> DonatEvents { get; set; }

        public virtual ICollection<GameExitEvent> GameExitEvents { get; set; }

        public virtual ICollection<GameLaunchEvent> GameLaunchEvents { get; set; }

        public virtual ICollection<ProcessAccelerationEvent> ProcessAccelerationEvents { get; set; }

        public virtual ICollection<ResourceHarvestEvent> ResourceHarvestEvents { get; set; }

        public virtual ICollection<ResourceSellingEvent> ResourceSellingEvents { get; set; }
    }
}