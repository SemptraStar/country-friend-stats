﻿using System;
using System.Linq;

namespace CountryFriendStatistics.EventGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("EventGenerator Start");

            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "GameLaunch"});
            EventProcessor.Program.Main(new string[] {"2", DateTime.Now.ToString(), "GameLaunch"});

            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "Ad"});

            EventProcessor.Program.Main(new string[] {"2", DateTime.Now.ToString(), "BuildDecorative", "Bush"});
            EventProcessor.Program.Main(new string[] {"2", DateTime.Now.ToString(), "BuildDecorative", "Bush"});
            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "BuildDecorative", "Fence"});
            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "BuildDecorative", "Pond"});

            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "Build", "Field"});
            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "Build", "Pigsty"});

            EventProcessor.Program.Main(new string[] {"3", DateTime.Now.ToString(), "Donut", "150"});
            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "Donut", "300"});
            EventProcessor.Program.Main(new string[] {"2", DateTime.Now.ToString(), "Donut", "75"});

            EventProcessor.Program.Main(new string[] {"2", DateTime.Now.ToString(), "ProcessAcceleration", "Building"});
            EventProcessor.Program.Main(new string[] {"2", DateTime.Now.ToString(), "ProcessAcceleration", "Quest"});

            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "ResourceHarvest", "Cow"});
            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "ResourceHarvest", "Apples"});

            EventProcessor.Program.Main(new string[] {"2", DateTime.Now.ToString(), "ResourceSelling", "Wheat"});
            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "ResourceSelling", "Wheat"});

            EventProcessor.Program.Main(new string[] {"1", DateTime.Now.ToString(), "GameExit"});
            EventProcessor.Program.Main(new string[] {"2", DateTime.Now.ToString(), "GameExit"});
            
            Console.WriteLine("EventGenerator End");
        }
    }
}
