﻿using System;

namespace CountryFriendStatistics.EventProcessor
{
    public enum EventType
    {
        Ad,
        BuildDecorative,
        Build,
        Donut,
        GameExit,
        GameLaunch,
        ProcessAcceleration,
        ResourceHarvest,
        ResourceSelling
    }
}
