﻿using System;
using System.Linq;
using CountryFriendStatistics.Database;
using CountryFriendStatistics.Database.Models.Events;
using CountryFriendStatistics.Database.Models.Events.Enums;

namespace CountryFriendStatistics.EventProcessor
{
    public class Program
    {
        static int _userId;
        static DateTime _eventTime;
        static EventType _eventType;

        public static void Main(string[] args)
        {
            Console.WriteLine("EventProcessor Start");

            // Processor uses at least 3 args:
            // 1. UserId
            // 2. EventTime
            // 3. EventType
            //
            // If the event requires more information,
            // the additional information read from args.

            if (args.Length < 3)
            {
                Console.WriteLine("EventProcessor failed to process given event.");
                return;
            }

            ParseEventData(args);
            AddGameEvent(args);

            Console.WriteLine("EventProcessor End");
        }

        static void ParseEventData(string[] args)
        {
            _userId = int.Parse(args[0]);
            _eventTime = DateTime.Parse(args[1]);
            _eventType = Enum.Parse<EventType>(args[2]);
        }

        static void AddGameEvent(string[] args)
        {
            var dbContextFactory = new DesignTimeDbContextFactory();

            using (var context = dbContextFactory.CreateDbContext(null))
            {
                if (!context.Users.Any(user => user.Id == _userId))
                {
                    Console.WriteLine($"User with ID = {_userId} not found.");
                    return;
                }

                switch (_eventType)
                {
                    case EventType.Ad:
                        {
                            context.AdEvents.Add(
                                new AdEvent
                                {
                                    UserId = _userId,
                                    EventTime = _eventTime
                                });

                            break;
                        }
                    case EventType.BuildDecorative:
                        {
                            var buildingType = Enum.Parse<BuildingType>(args[3]);

                            context.BuildDecorativeEvents.Add(
                                new BuildDecorativeEvent
                                {
                                    UserId = _userId,
                                    EventTime = _eventTime,
                                    BuildingType = buildingType
                                });

                            break;
                        }
                    case EventType.Build:
                        {
                            var buildingType = Enum.Parse<BuildingType>(args[3]);

                            context.BuildEvents.Add(
                                new BuildEvent
                                {
                                    UserId = _userId,
                                    EventTime = _eventTime,
                                    BuildingType = buildingType
                                });

                            break;
                        }
                    case EventType.Donut:
                        {
                            int amount = int.Parse(args[3]);

                            context.DonatEvents.Add(
                                new DonatEvent
                                {
                                    UserId = _userId,
                                    EventTime = _eventTime,
                                    Amount = amount
                                });

                            break;
                        }
                    case EventType.GameExit:
                        {
                            context.GameExitEvents.Add(
                                new GameExitEvent
                                {
                                    UserId = _userId,
                                    EventTime = _eventTime
                                });

                            break;
                        }
                    case EventType.GameLaunch:
                        {
                            context.GameLaunchEvents.Add(
                                new GameLaunchEvent
                                {
                                    UserId = _userId,
                                    EventTime = _eventTime
                                });

                            break;
                        }
                    case EventType.ProcessAcceleration:
                        {
                            var processType = Enum.Parse<ProcessType>(args[3]);

                            context.ProcessAccelerationEvents.Add(
                                new ProcessAccelerationEvent
                                {
                                    UserId = _userId,
                                    EventTime = _eventTime,
                                    ProcessType = processType
                                });

                            break;
                        }
                    case EventType.ResourceHarvest:
                        {
                            var resourceType = Enum.Parse<ResourceType>(args[3]);

                            context.ResourceHarvestEvents.Add(
                                new ResourceHarvestEvent
                                {
                                    UserId = _userId,
                                    EventTime = _eventTime,
                                    ResourceType = resourceType
                                });

                            break;
                        }
                    case EventType.ResourceSelling:
                        {
                            var resourceType = Enum.Parse<ResourceType>(args[3]);

                            context.ResourceSellingEvents.Add(
                                new ResourceSellingEvent
                                {
                                    UserId = _userId,
                                    EventTime = _eventTime,
                                    ResourceType = resourceType
                                });

                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Unknown event type");
                            break;
                        }
                }

                context.SaveChanges();
            }
        }
    }
}
